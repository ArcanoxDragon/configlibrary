﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using ConfigLibrary.Extensions;

namespace ConfigLibrary
{
	/// <summary>
	/// Handles loading, parsing, and saving of a configuration file
	/// </summary>
	public class ConfigFile
	{
		/// <summary>
		/// Loads a ConfigFile from the specified filename, building a default if necessary and if a BuildDefault delegate was specified
		/// </summary>
		/// <param name="fileName">The filename of the configuration file to read from</param>
		/// <param name="onBuildDefault">The method to be called if the configuration file does not exist and must be created</param>
		public ConfigFile( string fileName, Action<ConfigFile> onBuildDefault = null )
		{
			this.Name     = Path.GetFileNameWithoutExtension( fileName );
			this.RootNode = new Node( "root" );
			this.Filename = fileName;

			if ( File.Exists( this.Filename ) )
			{
				if ( this.LoadConfigFromFile() )
				{
					this.Loaded = true;
				}
				else
				{
					this.Loaded   = false;
					this.RootNode = new Node( "root" );
				}
			}
			else
			{
				var directoryName = Path.GetDirectoryName( fileName );

				if ( !Directory.Exists( directoryName ) )
					Directory.CreateDirectory( directoryName );

				File.Create( fileName ).Close();
				onBuildDefault?.Invoke( this );
				this.SaveConfig();
			}
		}

		/// <summary>
		/// Constructs a ConfigFile from a raw data stream
		/// </summary>
		/// <param name="name">Config file name</param>
		/// <param name="stream">Raw data stream</param>
		public ConfigFile( string name, Stream stream )
		{
			this.Name     = name;
			this.RootNode = new Node( "root" );
			this.Filename = null;

			if ( this.LoadConfigFromStream( stream ) )
			{
				this.Loaded = true;
			}
			else
			{
				this.Loaded   = false;
				this.RootNode = new Node( "root" );
			}
		}

		/// <summary>
		/// The name of this configuration file
		/// </summary>
		public string Name { get; }

		/// <summary>
		/// The filename on disk of this configuration file
		/// </summary>
		public string Filename { get; }

		/// <summary>
		/// The root Node of this configuration file
		/// </summary>
		public Node RootNode { get; }

		/// <summary>
		/// Whether or not this configuration file was successfully loaded
		/// </summary>
		public bool Loaded { get; }

		/// <summary>
		/// Handles loading this config from a file
		/// </summary>
		/// <returns></returns>
		private bool LoadConfigFromFile()
		{
			using ( var fs = new FileStream( this.Filename, FileMode.Open, FileAccess.ReadWrite ) )
			{
				try
				{
					if ( fs.CanRead )
					{
						var loaded = this.LoadConfigFromStream( fs );

						fs.Close();
						return loaded;
					}
				}
				catch ( IOException ex )
				{
					Console.WriteLine( "Error loading config: " );
					Console.WriteLine( ex.ToString() );
				}

				fs.Close();
				return false;
			}
		}

		/// <summary>
		/// Handles loading this config from a stream
		/// </summary>
		/// <param name="stream">The Stream to load from</param>
		/// <returns></returns>
		private bool LoadConfigFromStream( Stream stream )
		{
			using ( var r = new StreamReader( stream ) )
			{
				var lines = new List<string>();

				while ( !r.EndOfStream )
					lines.Add( r.ReadLine() );

				var curRoot = this.RootNode;

				for ( int i = 0; i < lines.Count; i++ )
				{
					if ( lines[ i ].Trim() == "" || lines[ i ].Trim().StartsWith( ";" ) )
						lines.RemoveAt( i-- );
				}

				for ( int i = 0; i < lines.Count; i++ )
				{
					var line = lines[ i ];

					if ( line.Contains( '=' ) ) // Key-value
					{
						var keyVal = line.Split( new[] { '=' }, 2 ); // Value can contain '='
						var key    = keyVal[ 0 ].Trim();
						var value  = keyVal[ 1 ].Trim();

						curRoot.SetValue( key, value );
					}
					else
					{
						// Beginning of group
						if ( line.Trim().EndsWith( "{" ) || ( i < lines.Count - 1 && lines[ i + 1 ].Trim() == "{" ) )
						{
							if ( i < lines.Count - 1 && lines[ i + 1 ].Trim() == "{" )
								i++;

							var nName = line.Trim().Replace( "{", "" );
							var node  = new Node( nName );

							curRoot.AddNode( node );
							curRoot = node;
						}
						// End of group
						else if ( line.Trim() == "}" )
						{
							// Too many group closes
							if ( curRoot.Parent == null )
							{
								Debug.WriteLine( "[{0}.cfg:{1}] Found \"}}\" but there are no groups left to close", this.Name, i + 1 );

								r.Close();
								return false;
							}

							curRoot = curRoot.Parent;
						}
						// Invalid line
						else
						{
							Debug.WriteLine( "[{0}.cfg:{1}] Invalid line: \"{2}\"", this.Name, i + 1, line );

							r.Close();
							return false;
						}
					}
				}

				// Not all groups were closed properly
				if ( curRoot != this.RootNode )
				{
					Debug.WriteLine( "[{0}.cfg:{1}] Unclosed group: {2}", this.Name, lines.Count, curRoot.Name );

					r.Close();
					return false;
				}

				r.Close();
				return true;
			}
		}

		/// <summary>
		/// Saves this config to a file
		/// </summary>
		public void SaveConfig()
		{
			if ( this.Filename == null )
				throw new InvalidOperationException( "Cannot save a config file loaded from an InputStream" );

			if ( File.Exists( this.Filename ) )
				File.Delete( this.Filename );

			using ( var fs = new FileStream( this.Filename, FileMode.Create, FileAccess.ReadWrite ) )
			using ( var w = new StreamWriter( fs ) )
			{
				foreach ( var node in this.RootNode.Children.Values )
					this.WriteNode( w, node, 0 );

				foreach ( var key in this.RootNode.Values.Keys )
					w.WriteLine( $"{key}={this.RootNode.Values[ key ]}" );
			}
		}

		/// <summary>
		/// Handles writing a node and, recursively, its children, to a file
		/// </summary>
		/// <param name="w">StreamWriter to use for writing the node</param>
		/// <param name="node">The node to write</param>
		/// <param name="indentation">The indentation level to write the node at</param>
		private void WriteNode( StreamWriter w, Node node, int indentation )
		{
			w.Write( "\t".Repeat( indentation ) );
			w.WriteLine( node.Name );
			w.Write( "\t".Repeat( indentation ) );
			w.WriteLine( "{" );

			foreach ( var child in node.Children.Values )
				this.WriteNode( w, child, indentation + 1 );

			foreach ( var key in node.Values.Keys )
			{
				w.Write( "\t".Repeat( indentation + 1 ) );
				w.WriteLine( $"{key}={node.Values[ key ]}" );
			}

			w.Write( "\t".Repeat( indentation ) );
			w.WriteLine( "}" );
		}

		/// <summary>
		/// Reads a value of type T from the config, if it exists
		/// </summary>
		/// <typeparam name="T">The type of value to read. Can be string, int, double, float, or bool</typeparam>
		/// <param name="path">Config path of the value to read (parent.child.value)</param>
		/// <param name="result">Output variable to write the value to if successful</param>
		/// <returns>True if the value was successfully read and parsed, false if it was unable to be read or does not exist</returns>
		public bool GetValue<T>( string path, out T result )
		{
			var nodes = path.Split( '.' );

			result = default;

			if ( nodes.Length <= 0 )
				return false;

			var key  = nodes[ nodes.Length - 1 ];
			var root = this.RootNode;

			for ( int i = 0; i < nodes.Length - 1; i++ )
			{
				if ( !root.HasChild( nodes[ i ] ) )
					return false;

				root = root.GetChild( nodes[ i ] );
			}

			if ( root != null )
			{
				if ( root.GetValue( key, out result ) )
					return true;
			}

			return false;
		}
	}
}