﻿namespace ConfigLibrary.Extensions
{
    public static class StringExtensions
    {
		/// <summary>
		/// Repeats this string a certain number of times
		/// </summary>
		/// <param name="original">The string to repeat</param>
		/// <param name="times">The number of times to repeat the string</param>
		/// <returns>This string, repeated the specified number of times</returns>
		public static string Repeat(this string original, int times)
		{
			string ret = "";
			for (int i = 0; i < times; i++)
			{
				ret += original;
			}
			return ret;
		}
    }
}
