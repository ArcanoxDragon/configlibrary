﻿using System;
using System.Collections.Generic;

namespace ConfigLibrary
{
	public class Node
	{
		public Node( string name )
		{
			this.Name     = name;
			this.Parent   = null;
			this.Children = new Dictionary<string, Node>();
			this.Values   = new Dictionary<string, string>();
		}

		public Node( string name, Node parent ) : this( name )
		{
			this.Parent = parent;
		}

		public string                     Name     { get; }
		public Node                       Parent   { get; set; }
		public Dictionary<string, Node>   Children { get; }
		public Dictionary<string, string> Values   { get; }

		public void AddNode( Node node )
		{
			node.Parent = this;
			this.Children.Add( node.Name, node );
		}

		public void SetValue( string key, object value )
		{
			if ( this.Values.ContainsKey( key ) )
				this.Values[ key ] = value.ToString();
			else
				this.Values.Add( key, value.ToString() );
		}

		public bool HasChild( string name ) => this.Children.ContainsKey( name );

		public Node GetChild( string name )
		{
			if ( !this.HasChild( name ) )
				return null;

			return this.Children[ name ];
		}

		public bool HasKey( string key ) => this.Values.ContainsKey( key );

		public bool GetValue<T>( string key, out T result )
		{
			if ( this.Values.ContainsKey( key ) )
			{
				var val = this.Values[ key ];

				if ( typeof( T ) == typeof( string ) )
				{
					result = (T) Convert.ChangeType( val, typeof( T ) );
					return true;
				}

				if ( typeof( T ) == typeof( int ) && int.TryParse( val, out var parsedInt ) )
				{
					result = (T) Convert.ChangeType( parsedInt, typeof( T ) );
					return true;
				}

				if ( typeof( T ) == typeof( double ) && double.TryParse( val, out var parsedDouble ) )
				{
					result = (T) Convert.ChangeType( parsedDouble, typeof( T ) );
					return true;
				}

				if ( typeof( T ) == typeof( float ) && float.TryParse( val, out var parsedFloat ) )
				{
					result = (T) Convert.ChangeType( parsedFloat, typeof( T ) );
					return true;
				}

				if ( typeof( T ) == typeof( bool ) && bool.TryParse( val, out var parsedBool ) )
				{
					result = (T) Convert.ChangeType( parsedBool, typeof( T ) );
					return true;
				}
			}

			result = default;
			return false;
		}
	}
}